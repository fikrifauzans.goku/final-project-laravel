@extends('layout.master')

@section('content')
<div class="class">
    <div class="main-content">
        <div class="class container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-body">
                        <form action="/siswa/{{$siswa->id}}/update" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama Depan</label>
                              <input type="text" name="nama_depan" class="form-control" id="exampleInputEmail1" aria-describedby="Nama Depan" value="{{$siswa->nama_depan}}">
                            </div>
            
                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama Belakang</label>
                              <input type="text" name="nama_belakang" class="form-control" id="exampleInputEmail1" aria-describedby="Nama Belakang" value="{{$siswa->nama_belakang}}">
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                                  <option value="L" @if ($siswa ->jenis_kelamin =='L') selected @endif>Laki-laki</option>
                                  <option value="P" @if ($siswa ->jenis_kelamin =='P') selected @endif>Perempuan</option>
                                </select>
                            </div>
                            
                              <div class="form-group">
                                <label for="exampleInputEmail1">Agama</label>
                                <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Agama" value="{{$siswa->agama}}">
                              </div>
                              <div class="form-group">
                                <label for="exampleFormControlTextarea1">Alamat</label>
                                <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$siswa->alamat}}</textarea>
                              </div>  
                              <div class="form-group">
                                <label for="exampleFormControlTextarea1">Avatar</label>
                                <input type="file" name="avatar" class="form-control">
                              </div>  
                              <button type="submit" class="btn btn-warning">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content1')
    
<h1>Edit Data Siswa</h1>
            <!-- Alret jika berhasil input-->
            @if (session('sukses'))
            <div class="alert alert-success" role="alert">
            Data Berhasil Dimasukkan!
            </div>
            @endif
        
        <div class="row">
            <div class="col-lg-12"> 
               
            </div>
        </div>
   

     
    
    
@endsection